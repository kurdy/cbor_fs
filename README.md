![Build status](https://img.shields.io/bitbucket/pipelines/kurdy/cbor_fs.svg) ![Bitbucket issues](https://img.shields.io/bitbucket/issues/kurdy/cbor_fs.svg) ![Licence](https://img.shields.io/crates/l/cbor_fs) ![Version](https://img.shields.io/crates/v/cbor_fs) [![dependency status](https://deps.rs/repo/bitbucket/kurdy/cbor_fs/status.svg)](https://deps.rs/repo/bitbucket/kurdy/cbor_fs) ![Download](https://img.shields.io/crates/d/cbor_fs)
# README #

# cbor_fs
A small lib that help to serialize and deserialize from/to cbor (Concise Binary Object Representation)  with a file. It does nothing special, just encapsulates serde_cbor into functions that can be used repeatedly.

## Example:

```rust
#[macro_use]
extern crate serde_derive;

use cbor_fs::{save_to,load_from};
use std::path::Path;

// Define a struct of data
#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct ExampleData {
    value: i64,
    message: String,
}

fn main() {
    let file_name = Path::new("test_fs.cbor");

    let data = ExampleData {
        value: -123456,
        message: "Example cbor_fs".into()
    };

    // Save to a file it return Result<(), Box<dyn Error>>
    save_to(file_name, &data).expect("Fail to save as cbor.");

    // Load from a file it return Result<T, Box<dyn Error>>
    let data2: ExampleData =load_from(file_name).expect("Fail to load from cbor.");
    println!("{:?}\n{:?}",data,data2);
}
```
